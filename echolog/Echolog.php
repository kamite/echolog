<?php
/**
 * Copyright (c) TOFFA DESIGN Software MIT License
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.2.9
 * @license       TOFFA DESIGN Software MIT License
 */

namespace Echolog\echolog;
use Cake\I18n\Time;
use Cake\I18n\FrozenTime;
use Cake\I18n\FrozenDate;
use Cake\I18n\Date;
use Cake\Chronos\Chronos;
use Cake\Chronos\ChronosInterval;
use Cake\Chronos\MutableDateTime;
use Cake\Chronos\MutableDate;
use Cake\Chronos\ChronosInterface;


/**
 * Help to calculate Medical values.
 *
 *
 * @link https://book.cakephp.org/3.0/en/core-libraries/inflector.html
 */
class Echolog
{
//date de début de grossesse
 public $ddg;
 public $ddgDay;
 public $now;
 public static $cycleregulier = 28;
 public $epf =array( // Données estimation du poids foetale
  "mentions"=>array(
    "titre"=>"Courbes estimation du poids foetale",
    "mention"=>"Selon le Collège Français d'Échographie Fœtale (CFEF)
    INSERM Unité 155"
  ),
 'p3'=>array(
   '17'=>148.35,
   '18'=>202.89,
   '19'=>251.15,
   '20'=>297.62,
   '21'=>346.17,
   '22'=>400.08,
   '23'=>461.98,
   '24'=>533.93,
   '25'=>617.35,
   '26'=>713.05,
   '27'=>821.24,
   '28'=>941.51,
   '29'=>1072.85,
   '30'=>1213.62,
   '31'=>1361.58,
   '32'=>1513.89,
   '33'=>1667.06,
   '34'=>1817.03,
   '35'=>1959.12,
   '36'=>2088.00,
   '37'=>2197.79,
   '38'=>2281.795
 ),
 'p10'=>array(
   '17'=>159.10,
   '18'=>215.61,
   '19'=>266.57,
   '20'=>316.4,
   '21'=>369.16,
   '22'=>427.94,
   '23'=>495.45,
   '24'=>573.72,
   '25'=>664.18,
   '26'=>767.66,
   '27'=>884.34,
   '28'=>1013.84,
   '29'=>1155.13,
   '30'=>1306.57,
   '31'=>1465.93,
   '32'=>1630.36,
   '33'=>1796.39,
   '34'=>1959.93,
   '35'=>2116.31,
   '36'=>2260.22,
   '37'=>2385.76,
   '38'=>2486.39
 ),
 'p50'=>array(
   '17'=>182.04,
   '18'=>242.75,
   '19'=>299.47,
   '20'=>356.66,
   '21'=>418.22,
   '22'=>487.39,
   '23'=>566.84,
   '24'=>658.60,
   '25'=>764.10,
   '26'=>884.16,
   '27'=>1018.97,
   '28'=>1168.15,
   '29'=>1330.66,
   '30'=>1504.87,
   '31'=>1688.55,
   '32'=>1878.84,
   '33'=>2072.28,
   '34'=>2264.78,
   '35'=>2451.66,
   '36'=>2627.63,
   '37'=>2786.76,
   '38'=>2922.53
 ),
 'p90'=>array(
   '17'=>204.98,
   '18'=>269.90,
   '19'=>332.37,
   '20'=>396.86,
   '21'=>467.27,
   '22'=>546.84,
   '23'=>638.23,
   '24'=>743.48,
   '25'=>864.01,
   '26'=>1000.65,
   '27'=>1153.60,
   '28'=>1322.45,
   '29'=>1506.19,
   '30'=>1703.17,
   '31'=>1911.17,
   '32'=>2127.32,
   '33'=>2348.17,
   '34'=>2569.63,
   '35'=>2787.02,
   '36'=>2995.03,
   '37'=>3187.76,
   '38'=>3358.68
 ),
 'p97'=>array(
   '17'=>215.72,
   '18'=>282.62,
   '19'=>347.79,
   '20'=>415.71,
   '21'=>490.26,
   '22'=>574.70,
   '23'=>671.69,
   '24'=>783.27,
   '25'=>910.85,
   '26'=>1055.26,
   '27'=>1216.71,
   '28'=>1394.78,
   '29'=>1588.47,
   '30'=>1796.13,
   '31'=>2015.52,
   '32'=>2243.60,
   '33'=>2477.49,
   '34'=>2712.53,
   '35'=>2944.21,
   '36'=>3167.25,
   '37'=>3375,73,
   '38'=>3563.12
 )
 );

public $lf =[
  "mentions"=>array(
    "titre"=>"Courbes longueur fémorale",
    "mention"=>"Selon le Collège Français d'Échographie Fœtale (CFEF)
    INSERM Unité 155"
  ),
	'p3' => [
		 12 => 2.76,
		 13 => 6.09,
		 14 => 9.40,
		 15 => 12.56,
		 16 => 15.70,
		 17 => 18.74,
		 18 => 21.69,
		 19 => 24.59,
		 20 => 27.42,
		 21 => 30.12,
		 22 => 32.83,
		 23 => 35.34,
		 24 => 37.89,
		 25 => 40.33,
		 26 => 42.66,
		 27 => 44.95,
		 28 => 47.13,
		 29 => 49.22,
		 30 => 51.30,
		 31 => 53.26,
		 32 => 55.12,
		 33 => 56.96,
		 34 => 58.69,
		 35 => 60.33,
		 36 => 61.90,
		 37 => 63.4,
		 38 => 64.81,
		 39 => 66.16,
		 40 => 67.42,
		 41 => 68.00
	],
	'p10' => [
		 12 => 3.89,
		 13 => 7.29,
		 14 => 10.65,
		 15 => 13.87,
		 16 => 17.00,
		 17 => 20.12,
		 18 => 23.14,
		 19 => 26.06,
		 20 => 28.94,
		 21 => 31.72,
		 22 => 34.39,
		 23 => 37.00,
		 24 => 39.58,
		 25 => 42.04,
		 26 => 44.40,
		 27 => 46.72,
		 28 => 48.94,
		 29 => 51.06,
		 30 => 53.14,
		 31 => 55.13,
		 32 => 57.04,
		 33 => 58.87,
		 34 => 60.62,
		 35 => 62.29,
		 36 => 63.89,
		 37 => 65.36,
		 38 => 66.79,
		 39 => 68.19,
		 40 => 69.47,
		 41 => 70.00
	],
	'p50' => [
		 12 => 6.33,
		 13 => 9.88,
		 14 => 13.33,
		 15 => 16.66,
		 16 => 19.95,
		 17 => 23.12,
		 18 => 26.23,
		 19 => 29.25,
		 20 => 32.23,
		 21 => 35.05,
		 22 => 37.87,
		 23 => 40.50,
		 24 => 43.16,
		 25 => 45.69,
		 26 => 48.17,
		 27 => 50.53,
		 28 => 52.80,
		 29 => 54.94,
		 30 => 57.13,
		 31 => 59.15,
		 32 => 61.11,
		 33 => 63.00,
		 34 => 64.76,
		 35 => 66.47,
		 36 => 68.13,
		 37 => 69.63,
		 38 => 71.11,
		 39 => 72.48,
		 40 => 73.79,
		 41 => 74.00
	],
	'p90' => [
		 12 => 8.79,
		 13 => 12.42,
		 14 => 16.00,
		 15 => 19.44,
		 16 => 22.80,
		 17 => 26.13,
		 18 => 29.30,
		 19 => 32.44,
		 20 => 35.48,
		 21 => 38.41,
		 22 => 41.30,
		 23 => 44.03,
		 24 => 46.75,
		 25 => 49.36,
		 26 => 51.88,
		 27 => 54.32,
		 28 => 56.64,
		 29 => 58.91,
		 30 => 61.08,
		 31 => 63.14,
		 32 => 65.19,
		 33 => 67.10,
		 34 => 68.88,
		 35 => 70.65,
		 36 => 72.34,
		 37 => 73.91,
		 38 => 75.38,
		 39 => 76.81,
		 40 => 78.14,
		 41 => 79.00
	],
	'p97' => [
		 12 => 10.00,
		 13 => 13.65,
		 14 => 17.27,
		 15 => 20.77,
		 16 => 24.18,
		 17 => 27.53,
		 18 => 30.80,
		 19 => 33.91,
		 20 => 37.03,
		 21 => 40.00,
		 22 => 42.91,
		 23 => 45.71,
		 24 => 48.42,
		 25 => 51.08,
		 26 => 53.62,
		 27 => 56.09,
		 28 => 58.45,
		 29 => 60.72,
		 30 => 62.92,
		 31 => 65.04,
		 32 => 67.07,
		 33 => 69.03,
		 34 => 70.84,
		 35 => 72.63,
		 36 => 74.30,
		 37 => 75.89,
		 38 => 77.41,
		 39 => 78.84,
		 40 => 80.17,
		 41 => 81.00
	]
  ];


 public $dtc = array( // Données diamètre transversale du cervelet
  "mentions"=>array(
    "titre"=>"Courbes diamètre transversale du cervelet",
    "mention"=>"Selon le Collège Français d'Échographie Fœtale (CFEF)
    INSERM Unité 155"
  ),
   'p5'=>array(
     '15'=>14.2,
     '16'=>14.6,
     '17'=>15.2,
     '18'=>15.9,
     '19'=>16.8,
     '20'=>17.7,
     '21'=>18.8,
     '22'=>19.9,
     '23'=>21.2,
     '24'=>22.5,
     '25'=>23.9,
     '26'=>25.3,
     '27'=>26.7,
     '28'=>28.2,
     '29'=>29.8,
     '30'=>31.3,
     '31'=>32.8,
     '32'=>34.4,
     '33'=>35.9,
     '34'=>37.3,
     '35'=>38.8,
     '36'=>40.1,
     '37'=>41.4,
     '38'=>42.7
   ),
   'p10'=>array(
     '15'=>14.4,
     '16'=>15.0,
     '17'=>15.6,
     '18'=>16.4,
     '19'=>17.3,
     '20'=>18.3,
     '21'=>19.4,
     '22'=>20.5,
     '23'=>21.8,
     '24'=>23.2,
     '25'=>24.6,
     '26'=>26.0,
     '27'=>27.6,
     '28'=>29.1,
     '29'=>30.7,
     '30'=>32.2,
     '31'=>33.8,
     '32'=>35.4,
     '33'=>37.0,
     '34'=>38.5,
     '35'=>40.0,
     '36'=>41.4,
     '37'=>42.8,
     '38'=>44.1
   ),
   'p50'=>array(
     '15'=>15.8,
     '16'=>16.5,
     '17'=>17.3,
     '18'=>18.2,
     '19'=>19.2,
     '20'=>20.4,
     '21'=>21.6,
     '22'=>23.0,
     '23'=>24.4,
     '24'=>25.9,
     '25'=>27.4,
     '26'=>29.1,
     '27'=>30.7,
     '28'=>32.4,
     '29'=>34.2,
     '30'=>35.9,
     '31'=>37.7,
     '32'=>39.5,
     '33'=>41.3,
     '34'=>43.1,
     '35'=>44.8,
     '36'=>46.5,
     '37'=>48.2,
     '38'=>49.9,
   ),
   'p90'=>array(
     '15'=>17.1,
     '16'=>17.9,
     '17'=>18.9,
     '18'=>19.9,
     '19'=>21.1,
     '20'=>22.4,
     '21'=>23.8,
     '22'=>25.3,
     '23'=>26.8,
     '24'=>28.5,
     '25'=>30.2,
     '26'=>31.9,
     '27'=>33.8,
     '28'=>35.6,
     '29'=>37.5,
     '30'=>39.5,
     '31'=>41.5,
     '32'=>43.4,
     '33'=>45.4,
     '34'=>47.4,
     '35'=>49.5,
     '36'=>51.4,
     '37'=>53.4,
     '38'=>55.4
   ),
   'p95'=>array(
     '15'=>17.4,
     '16'=>18.3,
     '17'=>19.3,
     '18'=>20.5,
     '19'=>21.7,
     '20'=>23.0,
     '21'=>24.5,
     '22'=>26.0,
     '23'=>27.6,
     '24'=>29.3,
     '25'=>31.0,
     '26'=>32.8,
     '27'=>34.7,
     '28'=>36.6,
     '29'=>38.6,
     '30'=>40.6,
     '31'=>42.6,
     '32'=>44.7,
     '33'=>46.7,
     '34'=>48.8,
     '35'=>50.9,
     '36'=>53.0,
     '37'=>55.0,
     '38'=>57.1,
   )

 );

public $pc = array(
  "mentions"=>array(
    "titre"=>"Courbes estimation du périmètre cranien",
    "mention"=>"Selon le Collège Français d'Échographie Fœtale (CFEF)
    INSERM Unité 155"
  ),
  "p3"=>array(
      "16"=>105.80,
      "17"=>118.67,
      "18"=>131.08,
      "19"=>143.00,
      "20"=>154.53,
      "21"=>165.41,
      "22"=>176.12,
      "23"=>186.32,
      "24"=>196.19,
      "25"=>205.50,
      "26"=>214.44,
      "27"=>222.87,
      "28"=>231.00,
      "29"=>238.40,
      "30"=>245.86,
      "31"=>252.54,
      "32"=>258.86,
      "33"=>264.62,
      "34"=>270.14,
      "35"=>275.33,
      "36"=>279.79,
      "37"=>283.90,
      "38"=>287.63,
      "39"=>290.88,
      "40"=>293.00,
  ),
  "p10"=>array(
      "16"=>110.58,
      "17"=>123.78,
      "18"=>136.36,
      "19"=>148.53,
      "20"=>160.21,
      "21"=>171.49,
      "22"=>182.35,
      "23"=>192.31,
      "24"=>203.00,
      "25"=>212.40,
      "26"=>221.57,
      "27"=>230.33,
      "28"=>238.56,
      "29"=>246.35,
      "30"=>253.74,
      "31"=>260.81,
      "32"=>267.22,
      "33"=>273.38,
      "34"=>279.00,
      "35"=>284.23,
      "36"=>289.00,
      "37"=>293.32,
      "38"=>297.29,
      "39"=>300.76,
      "40"=>303.00,
  ),
  "p50"=>array(
      "16"=>120.86,
      "17"=>134.49,
      "18"=>147.55,
      "19"=>160.29,
      "20"=>172.47,
      "21"=>184.21,
      "22"=>195.74,
      "23"=>206.64,
      "24"=>217.18,
      "25"=>227.32,
      "26"=>236.72,
      "27"=>246.00,
      "28"=>254.77,
      "29"=>263.00,
      "30"=>270.84,
      "31"=>278.33,
      "32"=>285.29,
      "33"=>292.00,
      "34"=>298.10,
      "35"=>303.62,
      "36"=>308.81,
      "37"=>313.52,
      "38"=>317.88,
      "39"=>321.86,
      "40"=>324.00,
  ),
  "p90"=>array(
      "16"=>131.25,
      "17"=>145.38,
      "18"=>158.92,
      "19"=>172.14,
      "20"=>184.86,
      "21"=>197.12,
      "22"=>208.91,
      "23"=>220.26,
      "24"=>231.39,
      "25"=>241.91,
      "26"=>252.00,
      "27"=>261.75,
      "28"=>271.00,
      "29"=>279.71,
      "30"=>288.13,
      "31"=>296.00,
      "32"=>303.54,
      "33"=>310.40,
      "34"=>317.00,
      "35"=>323.00,
      "36"=>328.75,
      "37"=>334.00,
      "38"=>338.64,
      "39"=>343.00,
      "40"=>346.00,
  ),
  "p97"=>array(
      "16"=>136.11,
      "17"=>150.39,
      "18"=>164.11,
      "19"=>177.48,
      "20"=>190.54,
      "21"=>203.09,
      "22"=>215.15,
      "23"=>226.76,
      "24"=>238.00,
      "25"=>248.81,
      "26"=>259.23,
      "27"=>269.13,
      "28"=>278.57,
      "29"=>287.56,
      "30"=>296.00,
      "31"=>304.27,
      "32"=>312.00,
      "33"=>319.10,
      "34"=>325.91,
      "35"=>332.16,
      "36"=>338.00,
      "37"=>343.34,
      "38"=>348.29,
      "39"=>352.67,
      "40"=>356.00,
  )

);
public $pa =[
  "mentions"=>array(
    "titre"=>"Courbes estimation du périmètre abdominale",
    "mention"=>"Selon le Collège Français d'Échographie Fœtale (CFEF)
    INSERM Unité 155"
  ),
    'p3' => [
      15 => 80.70,
      16 => 91.30,
      17 => 101.70,
      18 => 111.80,
      19 => 122.00,
      20 => 132.00,
      21 => 141.60,
      22 => 151.40,
      23 => 160.90,
      24 => 170.20,
      25 => 179.30,
      26 => 188.40,
      27 => 197.30,
      28 => 206.20,
      29 => 214.70,
      30 => 223.20,
      31 => 231.60,
      32 => 239.70,
      33 => 247.80,
      34 => 255.60,
      35 => 263.20,
      36 => 271.00,
      37 => 278.30,
      38 => 285.60,
      39 => 292.70,
      40 => 298.00
    ],
    'p10' => [
      15 => 85.30,
      16 => 96.10,
      17 => 106.80,
      18 => 117.40,
      19 => 128.00,
      20 => 138.00,
      21 => 148.20,
      22 => 158.20,
      23 => 168.20,
      24 => 177.80,
      25 => 187.30,
      26 => 196.70,
      27 => 206.00,
      28 => 215.10,
      29 => 224.00,
      30 => 232.80,
      31 => 241.60,
      32 => 250.00,
      33 => 258.40,
      34 => 266.70,
      35 => 274.70,
      36 => 282.60,
      37 => 290.30,
      38 => 298.00,
      39 => 305.30,
      40 => 311.00
    ],
    'p50' => [
      15 => 95.00,
      16 => 106.40,
      17 => 118.00,
      18 => 129.20,
      19 => 140.40,
      20 => 151.40,
      21 => 162.30,
      22 => 173.00,
      23 => 183.60,
      24 => 194.00,
      25 => 204.40,
      26 => 214.50,
      27 => 224.50,
      28 => 234.40,
      29 => 244.00,
      30 => 253.60,
      31 => 263.00,
      32 => 272.20,
      33 => 281.20,
      34 => 290.20,
      35 => 298.80,
      36 => 307.40,
      37 => 316.00,
      38 => 324.70,
      39 => 332.40,
      40 => 339.00
    ],
    'p90' => [
      15 => 104.40,
      16 => 116.80,
      17 => 129.00,
      18 => 141.00,
      19 => 153.00,
      20 => 164.70,
      21 => 176.30,
      22 => 187.80,
      23 => 199.00,
      24 => 210.30,
      25 => 221.30,
      26 => 232.30,
      27 => 243.00,
      28 => 253.60,
      29 => 264.00,
      30 => 274.20,
      31 => 284.20,
      32 => 294.30,
      33 => 304.00,
      34 => 313.80,
      35 => 323.30,
      36 => 332.50,
      37 => 341,70,
      38 => 350.70,
      39 => 359.60,
      40 => 367.00
    ],
    'p97' => [
      15 => 108.80,
      16 => 121.60,
      17 => 134.00,
      18 => 146.60,
      19 => 158.80,
      20 => 171.00,
      21 => 183.00,
      22 => 194.70,
      23 => 206.30,
      24 => 218.00,
      25 => 229.30,
      26 => 240.60,
      27 => 251.60,
      28 => 262.60,
      29 => 273.30,
      30 => 283.70,
      31 => 294.40,
      32 => 304.60,
      33 => 314.80,
      34 => 324.80,
      35 => 334.50,
      36 => 344.30,
      37 => 353.80,
      38 => 363.00,
      39 => 372.20,
      40 => 380.00
    ]
];
public $mbip = array(
  "mentions"=>array(
    "titre"=>"Courbes estimation du BIP",
    "mention"=>"Selon le Collège Français d'Échographie Fœtale (CFEF)
    INSERM Unité 155"
  ),
  "p3"=>array(
        "11"=>12.08,
        "12"=>15.81,
        "13"=>19.47,
        "14"=>23.05,
        "15"=>26.56,
        "16"=>29.97,
        "17"=>33.32,
        "18"=>36.55,
        "19"=>39.76,
        "20"=>42.85,
        "21"=>45.86,
        "22"=>48.79,
        "23"=>51.63,
        "24"=>54.38,
        "25"=>57.04,
        "26"=>59.62,
        "27"=>62.12,
        "28"=>64.50,
        "29"=>66.84,
        "30"=>69.07,
        "31"=>71.22,
        "32"=>73.30,
        "33"=>75.24,
        "34"=>77.14,
        "35"=>78.94,
        "36"=>80.64,
        "37"=>82.27,
        "38"=>83.78,
        "39"=>85.22,
        "40"=>86.57,
        "41"=>87.00
    ),
    "p10"=>array(
        "11"=>13.12,
        "12"=>16.96,
        "13"=>20.71,
        "14"=>24.36,
        "15"=>27.93,
        "16"=>31.41,
        "17"=>34.85,
        "18"=>38.15,
        "19"=>41.46,
        "20"=>44.56,
        "21"=>47.66,
        "22"=>50.61,
        "23"=>53.48,
        "24"=>56.31,
        "25"=>59.00,
        "26"=>61.64,
        "27"=>64.15,
        "28"=>66.61,
        "29"=>68.98,
        "30"=>71.21,
        "31"=>73.39,
        "32"=>75.49,
        "33"=>77.46,
        "34"=>79.36,
        "35"=>81.14,
        "36"=>82.88,
        "37"=>84.50,
        "38"=>86.00,
        "39"=>87.43,
        "40"=>88.78,
        "41"=>89.00,
    ),
    "p50"=>array(
        "11"=>15.36,
        "12"=>19.40,
        "13"=>23.30,
        "14"=>27.14,
        "15"=>30.89,
        "16"=>34.53,
        "17"=>38.12,
        "18"=>41.58,
        "19"=>45.00,
        "20"=>48.22,
        "21"=>51.43,
        "22"=>54.53,
        "23"=>57.51,
        "24"=>60.42,
        "25"=>63.25,
        "26"=>65.94,
        "27"=>68.55,
        "28"=>71.03,
        "29"=>73.50,
        "30"=>75.80,
        "31"=>78.00,
        "32"=>80.16,
        "33"=>82.14,
        "34"=>84.07,
        "35"=>85.90,
        "36"=>87.61,
        "37"=>89.24,
        "38"=>90.70,
        "39"=>92.10,
        "40"=>93.45,
        "41"=>94.00,
    ),
    "p90"=>array(
        "11"=>17.60,
        "12"=>21.81,
        "13"=>25.92,
        "14"=>29.92,
        "15"=>33.82,
        "16"=>37.62,
        "17"=>41.35,
        "18"=>44.97,
        "19"=>48.52,
        "20"=>51.90,
        "21"=>55.23,
        "22"=>58.44,
        "23"=>61.54,
        "24"=>64.57,
        "25"=>67.48,
        "26"=>70.24,
        "27"=>72.92,
        "28"=>75.52,
        "29"=>77.97,
        "30"=>80.37,
        "31"=>82.63,
        "32"=>84.80,
        "33"=>86.84,
        "34"=>88.80,
        "35"=>90.61,
        "36"=>92.35,
        "37"=>93.97,
        "38"=>95.42,
        "39"=>96.86,
        "40"=>98.13,
        "41"=>99.00
    ),
    "p97"=>array(
        "11"=>18.63,
        "12"=>22.92,
        "13"=>27.12,
        "14"=>31.23,
        "15"=>35.23,
        "16"=>39.08,
        "17"=>42.87,
        "18"=>46.56,
        "19"=>50.18,
        "20"=>53.64,
        "21"=>57.00,
        "22"=>60.30,
        "23"=>63.45,
        "24"=>66.50,
        "25"=>69.42,
        "26"=>72.27,
        "27"=>75.00,
        "28"=>77.60,
        "29"=>80.09,
        "30"=>82.52,
        "31"=>84.80,
        "32"=>87.00,
        "33"=>89.04,
        "34"=>91.00,
        "35"=>92.83,
        "36"=>94.56,
        "37"=>96.19,
        "38"=>97.66,
        "39"=>99.05,
        "40"=>100.31,
        "41"=>101.00
    )

);





 public $termeSaLcc= array(
   "lcc/sa"=>array(
     '42'=>"10 weeks and 5 days",
     '43'=>"10 weeks and 6 days",
     '44'=>"11 weeks and 0 days",
     '45'=>"11 weeks and 0 days",
     '46'=>"11 weeks and 1 days",
     '47'=>"11 weeks and 1 days",
     '48'=>"11 weeks and 2 days",
     '49'=>"11 weeks and 3 days",
     '50'=>"11 weeks and 3 days",
     '51'=>"11 weeks and 4 days",
     '52'=>"11 weeks and 4 days",
     '53'=>"11 weeks and 5 days",
     '54'=>"11 weeks and 5 days",
     '55'=>"11 weeks and 6 days",
     '56'=>"11 weeks and 6 days",
     '57'=>"12 weeks and 0 days",
     '58'=>"12 weeks and 1 days",
     '59'=>"12 weeks and 1 days",
     '60'=>"12 weeks and 2 days",
     '61'=>"12 weeks and 2 days",
     '62'=>"12 weeks and 3 days",
     '64'=>"12 weeks and 4 days",
     '65'=>"12 weeks and 4 days",
     '66'=>"12 weeks and 5 days",
     '67'=>"12 weeks and 5 days",
     '68'=>"12 weeks and 6 days",
     '69'=>"12 weeks and 6 days",
     '70'=>"13 weeks and 0 days",
     '71'=>"13 weeks and 0 days",
     '72'=>"13 weeks and 1 days",
     '73'=>"13 weeks and 1 days",
     '74'=>"13 weeks and 1 days",
     '75'=>"13 weeks and 2 days",
     '76'=>"13 weeks and 2 days",
     '77'=>"13 weeks and 3 days",
     '78'=>"13 weeks and 3 days",
     '79'=>"13 weeks and 4 days",
     '80'=>"13 weeks and 4 days",
     '81'=>"13 weeks and 5 days",
     '82'=>"13 weeks and 5 days",
     '83'=>"13 weeks and 6 days",
     '84'=>"13 weeks and 6 days",
   ),
   'mention'=>'Robinson HP.Fleming JEE, Acritical evaluation of sonar "crown - rump lenght" measurement'
 );
public function __construct($ddg=null){
$this->ddg = $ddg;
$this->ddgDay = new Date($this->ddg);
$this->ddgDay = $this->ddgDay->format('l');
$this->now = Chronos::now();
}
 public function epf(){
  return $this->epf;
 }
 public function lccSa(){
   return $this->termeSaLcc["lcc/sa"];
 }
 public function epf10(){
   return $this->epf['p10'];
 }
 public function epf3(){
   return $this->epf['p3'];
 }
 public static function epf50(){
   return self::epf['p50'];
 }
 public static function epf90(){
   return self::epf['p90'];
 }
 public static function epf97(){
   return self::epf['p97'];
 }

  public static function imc($poids,$taille){
    if(is_int($poids) && is_int($taille)){
    return $poids/($taille*$taille);
  }else{
    return "100";
  }

  } //calcul de l'IMC

  public static function imcToString($poids,$taille){
    $imc =($poids/($taille*$taille));
    if($imc<16.5){
      return __("Famine");
    }elseif($imc>=16.5 && $imc<=18.5){
      return __("Maigreur");
    }elseif($imc>=18.5 && $imc<=25){
      return __("corpulence normale");
    }elseif($imc>=18.5 && $imc<=25){
      return __("corpulence normale");
    }elseif($imc>=25 && $imc<=30){
      return __("Surpoids");
    }elseif($imc>=30 && $imc<=35){
      return __("Obésité modérée");
    }elseif($imc>=35 && $imc<=40){
      return __("Obésité sévère");
    }elseif($imc>40){
      return __("obésité morbide ou massive");
    }

  } // calcul de l'IMC et resultat en chaine de charactere
    static function weeks_between($datefrom, $dateto)
    {
        $datefrom = DateTime::createFromFormat('d/m/Y H:i:s',$datefrom);
        $dateto = DateTime::createFromFormat('d/m/Y H:i:s',$dateto);
        $interval = $datefrom->diff($dateto);
        $week_total = $interval->format('%a')/7;
        return floor($week_total);

    }
  public function naegele(){
    $ddg = new Date ($this->ddg);
    $ddg->addDay(14);
    $ddg->addMonth(9);
    return $ddg;
  }

  public function previousDay(Date $date, Date $ddg){
      $dayInLetter= strtolower($ddg->format('l'));
      switch ($dayInLetter) {
        case 'monday':
        $naegele = $date->previous(ChronosInterface::MONDAY);
          break;
        case 'tuesday':
        $naegele = $date->previous(ChronosInterface::TUESDAY);
          break;
        case 'wednesday':
          //var_dump($date);
          $naegele = $date->previous(ChronosInterface::WEDNESDAY);
          break;
        case 'thursday':
        $naegele = $date->previous(ChronosInterface::THURSDAY);
          break;
        case 'friday':
        $naegele = $date->previous(ChronosInterface::FRIDAY);
          break;
        case 'saturday':
        $naegele = $date->previous(ChronosInterface::SATURDAY);
          break;
        case 'sunday':
        $naegele = $date->previous(ChronosInterface::SUNDAY);
          break;
        
        default:
          return $naegele;
          break;
      }
      return $naegele;
  }

  public function pregnancyEndWithDdc(string $ddc){
  $ddc = new Date($ddc);
  $rules =new Chronos($ddc);  
  $dfg = $ddc->addMonth(9);
  return $result = ['ddg'=>$ddc->nice('Europe/Paris', 'fr-FR'),'pe'=>$dfg,'day'=>$rules->diffInDays($this->now),'sa'=>floor($rules->diffInDays($this->now,true)/7),'saday'=>floor($rules->diffInDays($this->now))%7];
  }

  public function pregnancyEnd($cycle=null){
    $rules =new Chronos($this->ddg);  
    $ddg = new Date($this->ddg);
    if(isset($cycle) && $cycle !=0){
        $cycle_day = $cycle - self::$cycleregulier; 
        $dayInLetter= strtoupper($ddg->format('l'));
        if($cycle<0){
            $dfg = $this->naegele();
            $dfg->subDay(abs($cycle_day));

        }else{
          $dfg = $this->naegele();
          $dfg->addDay(abs($cycle_day));
        }

    }else{
      $dfg = $this->naegele();
    }
    if($dfg->format('l')!=$this->ddgDay){
      $dfg =$this->previousDay($dfg,$ddg);

    }
    
    if($dfg->isPast()){
      return ["response"=>false,"message"=>"Date d'accouchement dépassée!",'ddg'=>$ddg->nice('Europe/Paris', 'fr-FR'),'pe'=>$dfg,'day'=>$rules->diffInDays($this->now),'sa'=>floor($rules->diffInDays($this->now,true)/7),'saday'=>floor($rules->diffInDays($this->now))%7];
    }else{
    return ['ddg'=>$ddg->nice('Europe/Paris', 'fr-FR'),'pe'=>$dfg,'day'=>$rules->diffInDays($this->now),'sa'=>floor($rules->diffInDays($this->now,true)/7),'saday'=>floor($rules->diffInDays($this->now))%7];
    }
    

  } // calcul de l'âge de la grossesse
  public static function ddgm($ddgm){
  return ['ddg'=>$ddgm->nice('Europe/Paris', 'fr-FR'),'pe'=>$ddgm,'day'=>$ddgm->diffInDays(self::now),'sa'=>floor($ddgm->diffInDays(self::now,true)/7),'saday'=>floor($ddgm->diffInDays(self::now))%7];

  }

  public static function getAge($birthday){
    $now = Date::now();
    $year = $now->year;
    $birthday = new Date($birthday);
    return $year-$birthday->year.' '.__('Ans') ;
  }  // calcul de l'age retour du type: 20 Ans

  public static function hadlockCfe($pc,$pa,$lf,$bip){
  $epf = 1.326 + 0.0107*$pc + 0.0438*$pa + 0.158*$lf + 0.00326*$pa + 0.00326*$lf;
  return 10 ** $epf;
  }

}
