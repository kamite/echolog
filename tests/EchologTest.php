<?php
declare(strict_types=1);
use Echolog\echolog\Echolog;
use Cake\I18n\Date;
require_once("vendor/autoload.php");
use PHPUnit\Framework\TestCase;
class EchologTest extends TestCase{
    private $poids =50;
    private  $taille = 150;
    private $ddgTest;
    private $echolog;
    protected function setUp():void
    {
       $this->echolog = new Echolog("17-10-2019");
       $this->ddgTest = new Date($this->echolog->ddg);
    }

    public function testPregnancyEnd(){
        // on test si le resultat de pregnancy end est bien un tableau
        $dfg = $this->echolog->pregnancyEnd(28);
        // on vévifie si le tableau retourné contient bien les bonne clés
        $this->assertArrayHasKey("pe",$dfg);
        $this->assertArrayHasKey("ddg",$dfg);
        $this->assertArrayHasKey("sa",$dfg);
        $this->assertArrayHasKey("day",$dfg);
        $this->assertArrayHasKey("saday",$dfg);
        // on vérifie si la clé pe est bien associé à la date de début des premières règles
        $this->assertEquals($dfg["ddg"],$this->ddgTest->nice('Europe/Paris', 'fr-FR') );
        
    }

    public function testNaegele(){
     $naegele = $this->echolog->naegele();
     // on vérifie que la méthode nous retourne bien un objet d'instance Date
     $this->assertInstanceOf(Date::class, $naegele);
     // on vérifie qu'on retrouve bien la date de début de grossesse en partant de la date de fin de grossesse fournie par naegele
     $this->assertEquals($this->ddgTest,$naegele->subDay(14)->subMonth(9));
     
    }


}
